import sublime_plugin


class SwitcherCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        sels = self.view.sel()
        for sel in sels:
            name = self.view.substr(sel)
            parts = name.split('_')
            lead = parts[0]

            if len(parts) > 1:
                for x in range(1, len(parts)):
                    lead += parts[x].title()

                self.view.replace(edit, sel, lead)
            else:
                for index, letter in enumerate(name):
                    if letter.isupper() and index > 0:
                        name = name.replace(letter, '_%s' % letter.lower())

                self.view.replace(edit, sel, name)
